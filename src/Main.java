import json.*;

import java.io.*;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Vector;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Введите \"connect\", чтобы подключиться к серверу");
            try {
                String s = scanner.next();
                if (s.equals("exit")) System.exit(0);
                if (!s.equals("connect")) continue;
            } catch (NoSuchElementException e) {
                System.out.println("Ой!!!");
                break;
            }
            Socket socket;
            try {
                socket = connect(Integer.parseInt(args[0]));
            } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
                socket = connect(8900);
//                socket = connect(1488);
            }
            if (socket == null) {
                continue;
            }

            OutputStream outputStream = null;
            InputStream inputStream = null;
            try {
                outputStream = socket.getOutputStream();
                inputStream = socket.getInputStream();
            } catch (IOException e) {
                System.out.println("Невозможно получить поток вывода!");
                System.exit(-1);
            }
            ObjectInputStream objectInputStream = null;
            ObjectOutputStream objectOutputStream = null;
            DataInputStream din = new DataInputStream(inputStream);
            DataOutputStream dOUT = new DataOutputStream(outputStream);
            try {
                objectInputStream = new ObjectInputStream(inputStream);
                objectOutputStream = new ObjectOutputStream(outputStream);

            } catch (NullPointerException | IOException e) {
                System.out.println("Coeдинение не установлено");
                continue;
            }

            try {
                System.out.println("Соединение установлено");
                while (true) {
                    String sss = objectInputStream.readUTF();
                    System.out.println(sss);
                    if (sss.equals("До свидания!")) {
                        inputStream.close();
                        outputStream.close();
                        socket.close();
                        break;
                    }
                    if (sss.equals("Введите команду")) {
                        try {
                            String[] ss = readAndParse();
                            Picture picture = prepare(ss);
                            objectOutputStream.writeUTF(ss[0]);
                            objectOutputStream.flush();
                            switch (ss[0]) {
                                case "insert":
                                    if (ss.length < 3) {
                                        System.out.println("Нужен аргумент");
                                        objectOutputStream.writeUTF("");
                                        objectOutputStream.flush();
                                        objectOutputStream.writeObject(null);
                                        objectOutputStream.flush();
                                        break;
                                    }
                                    objectOutputStream.writeUTF(ss[1]);
                                    objectOutputStream.flush();
                                    objectOutputStream.writeObject(picture);
                                    objectOutputStream.flush();
                                    break;
                                case "remove":
                                    if (ss.length < 2) {
                                        System.out.println("Нужен аргумент");
                                        objectOutputStream.writeUTF("remove_arg_lost");
                                        objectOutputStream.flush();
                                        break;
                                    } else
                                        objectOutputStream.writeObject(picture);
                                    objectOutputStream.flush();
                                    break;
                                case "add":
                                    if (ss.length < 2) {
                                        System.out.println("Нужен аргумент");
                                        objectOutputStream.writeUTF("add_arg_lost");
                                        objectOutputStream.flush();
                                        break;
                                    }
                                    objectOutputStream.writeObject(picture);
                                    objectOutputStream.flush();
                                    break;
                                case "add_if_min":
                                    if (ss.length < 2) {
                                        System.out.println("Нужен аргумент");
                                        objectOutputStream.writeUTF("add_if_min_arg_lost");
                                        objectOutputStream.flush();
                                        break;
                                    }
                                    objectOutputStream.writeObject(picture);
                                    objectOutputStream.flush();
                                    break;
                                case "show":
                                    try {
                                       Vector<Picture> pics =  (Vector<Picture>)objectInputStream.readObject();
                                    if (pics != null){
                                        System.out.println(pics);
                                    }
                                    }
                                    catch (ClassNotFoundException e){
                                        System.out.println("не пришла картина");
                                    }
                                case "import":
                                    if (ss.length < 2) {
                                        System.out.println("Нужен аргумент");
                                        objectOutputStream.writeUTF("import_arg_lost");
                                        objectOutputStream.flush();
                                        break;
                                    }
                                    String _filename = ss[1];
                                    System.out.println(_filename);
                                    int k = 0;
                                    InputStreamReader reader = new InputStreamReader(new FileInputStream(_filename));

                                    StringBuilder builder = new StringBuilder();

                                    try {
                                        while (k != -1) {
                                            k = reader.read();
                                            char c = (char) k;
                                            builder.append(c);
                                        }
                                        builder.deleteCharAt(builder.length() - 1);
                                        System.out.println(builder.toString());
                                        objectOutputStream.writeUTF(builder.toString());
                                        objectOutputStream.flush();
                                        reader.close();
                                        break;
                                    } catch (IOException e) {
                                        System.out.println("ошибка файловой системы при попытке загрузки данных из файла");
                                        objectOutputStream.writeUTF("import_args_state");
                                        objectOutputStream.flush();
                                        break;
                                    }
                            }

                        } catch (NoSuchElementException e) {
                            break;
                        }
                    }
                }
            } catch (IOException e) {
                System.out.println("не получается достучаться до файла");
//                System.out.println("Сервер недоступен для отправки");
//                e.printStackTrace();

            } catch (NullPointerException e) {
                System.out.println("упссссс");

            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Попробуйте еще раз");
                e.printStackTrace();
            }
//            catch (ClassNotFoundException e ){
//                System.out.println("проблема с объектом");
//            }

        }
    }

    private static Socket connect(int i) {
        try {
            Socket socket = new Socket("localhost", i);

            return socket;
        } catch (Exception e) {
            System.out.println("не удалось установить соединение");
        }
        return null;
    }


    private static Picture prepare(String[] Command) {
        if (Command[0].equals("remove") && Command.length == 1) return null;
        if (Command[0].equals("insert") || Command[0].equals("remove") || Command[0].equals("add") || Command[0].equals("add_if_min") || Command[0].equals("import")) {
            if (Command[0].equals("insert") && Command.length < 3) {
                System.out.println("Недостаточно аргументов");
                return null;
            }
            if ((Command[0].equals("add") || Command[0].equals("add_if_min") || Command[0].equals("remove") || Command[0].equals("import")) && Command.length < 2) {
                System.out.println("Недостаточно аргументов");
                return null;
            }
            if (Command[0].equals("insert") && (Command[1].contains("{") || Command[1].contains("}"))) {
                System.out.println("индекс не ожидает фигурных скобок, задайте его, пожалуйста, без них");
                return null;
            }
            if (Command[0].equals("insert") && (Integer.parseInt(Command[1]) < 0)) {
                System.out.println("индекс не не может быть отрицательным");
                return null;
            }
            if (Command[0].equals("add") || Command[0].equals("add_if_min") || Command[0].equals("remove") || Command[0].equals("insert")) {
                String element;
                if (Command[0].equals("insert")) {
                    if (Command.length < 3) {
                        System.out.println("нужен индекс и картина");
                        return null;
                    } else
                        element = Command[2].trim();

                } else {
                    element = Command[1].trim();
                }
                try {
                    element = element.replaceAll(" ", "");
                    JSONEntity entity = JSONParser.parse(element);

                    if (!entity.isObject()) {
                        System.out.println("Данный json должен быть объектом, но имеет тип " + entity.getType().getRussianName());
                        return null;
                    }

                    JSONObject object = (JSONObject) entity;
                    JSONEntity nameEntity = object.getItem("name");
                    JSONEntity creatorEntity = object.getItem("creator");
                    String name = "";
                    String creator = "";

                    if (nameEntity != null) {
                        if (nameEntity.isString()) {
                            name = ((JSONString) nameEntity).getContent();
                            System.out.println("name" + name);
                        } else {
                            System.out.println("name должен быть строкой, но имеет тип " + nameEntity.getType().getRussianName());
                            return null;
                        }
                    }

                    if (creatorEntity != null) {
                        if (creatorEntity.isString()) {
                            creator = ((JSONString) creatorEntity).getContent();
                            System.out.println("creator" + creator);
                        } else {
                            System.out.println("creator должен быть строкой, но имеет тип " + creatorEntity.getType().getRussianName());
                            return null;
                        }
                    }

                    return new Picture(new Creator(creator), name);
                } catch (NullPointerException ex) {
                    System.out.println("Ошибка, элемент задан неверно");
                    return null;
                } catch (JSONParseException e) {
                    System.out.println(element);
                    System.out.println("не удалось распознать элемент");
                    return null;
                }

            }
        }
        return null;
    }

    private static String[] readAndParse() {

        Scanner scanner = new Scanner(System.in);
        String command = "";

        do command = command + scanner.nextLine();
        while ((command.length() - command.replaceAll("}", "").length()) < (command.length() - command.replaceAll("\\{", "").length()));
        //ввели пока строчка не содержит равное количество скобочек

        command = command.trim();

        String[] fullCommand;
        if (!command.split(" ", 2)[0].equals("insert")) {
            fullCommand = command.split(" ", 2); // делим по первому пробелу
            if (fullCommand.length > 1)
                while (fullCommand[1].contains("  ")) fullCommand[1] = fullCommand[1].replaceAll("  ", " ");
            if (fullCommand[0].equals("import")) {
                if (fullCommand.length > 1) {
                    fullCommand[1] = fullCommand[1].replaceAll(" ", "");
                    System.out.println(fullCommand[1]);
                }

            }
        } else {
            if (command.split(" ", 3).length < 3)
                fullCommand = command.split(" ", 2);
            else {
                fullCommand = command.split(" ", 3);
                while (fullCommand[1].contains("  ")) fullCommand[1] = fullCommand[1].replaceAll("  ", " ");
            }
        }
        return fullCommand;

    }
}