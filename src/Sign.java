public class Sign {

    protected String _creator;
    protected String _name;

    Sign(Picture picture) {

        set_creator(picture.getCreator());

        set_name(picture.getName());

    }

    @Override
    public String toString() {
        return this.get_name() + "\n" + this.get_creator();
    }


    private void set_creator(String _creator) {
        this._creator = "Художник " + _creator;
    }

    private void set_name(String _name) {
        this._name = "Название картины " + _name;
    }


    public String get_creator() {
        return _creator;
    }

    public String get_name(){
        return _name;
    }
}



